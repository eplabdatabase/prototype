<div><p>Lorem ipsum dolor sit amet tellus.</p></div>
<div><blockquote>Etiam quis nulla pretium et.</blockquote></div>
<div><img src="images/inset.png" alt="Inset Image" /></div>
<ul>
<li><p>Lorem ipsum dolor sit amet tellus.</p></li>
<li><blockquote>Etiam quis nulla pretium et.</blockquote></li>
<li><img src="images/inset.png" alt="Inset Image" /></li>
</ul>

<?php
header("Content-type: text/css; charset: UTF-8");

$divData = array(
 'width' => '550',
 'height' => '250',
);
$liData = array(
 'width' => '600',
 'height' => '300',
);
$blockquoteData = array(
 'width' => '440',
 'height' => '100'
);
$imgData = array(
 'width' => '450',
 'height' => '150'
);
$pData = array(
 'width' => '480',
 'height' => '130'
);
?>



div {
 width: <?=$divData['width']?>px;
 height: <?=$divData['height']?>px;
}
li {
 width: <?=$liData['width']?>px;
 height: <?=$liData['height']?>px;
}
div blockquote {
 width: <?=$blockquoteData['width']?>px;
 height: <?=$blockquoteData['height']?>px;
 <?
  $blockquoteData['divMarginX'] = $divData['width']-$blockquoteData['width'];
  $blockquoteData['divMarginY'] = $divData['height']-$blockquoteData['height'];
 ?>
 margin: <? echo blockquoteData['divMarginY']/2; ?>px <? echo blockquoteData['divMarginX']/2; ?>px;
}
div img {
 width: <?=$imgData['width']?>px;
 height: <?=$imgData['height']?>px;
 <?
  $imgData['divMarginX'] = $divData['width']-$imgData['width'];
  $imgData['divMarginY'] = $divData['height']-$imgData['height'];
 ?>
 margin: <? echo imgData['divMarginY']/2; ?>px <? echo imgData['divMarginX']/2; ?>px;
}
div p {
 width: <?=$pData['width']?>px;
 height: <?=$pData['height']?>px;
 <?
  $pData['divMarginX'] = $divData['width']-$pData['width'];
  $pData['divMarginY'] = $divData['height']-$pData['height'];
 ?>
 margin: <? echo pData['divMarginY']/2; ?>px <? echo pData['divMarginX']/2; ?>px;
}
li blockquote {
 width: <?=$blockquoteData['width']?>px;
 height: <?=$blockquoteData['height']?>px;
 <?
  $blockquoteData['liMarginX'] = $liData['width']-$blockquoteData['width'];
  $blockquoteData['liMarginY'] = $liData['height']-$blockquoteData['height'];
 ?>
 margin: <? echo blockquoteData['liMarginY']/2; ?>px <? echo blockquoteData['liMarginX']/2; ?>px;
}
li img {
 width: <?=$imgData['width']?>px;
 height: <?=$imgData['height']?>px;
 <?
  $imgData['liMarginX'] = $liData['width']-$imgData['width'];
  $imgData['liMarginY'] = $liData['height']-$imgData['height'];
 ?>
 margin: <? echo imgData['liMarginY']/2; ?>px <? echo imgData['liMarginX']/2; ?>px;
}
li p {
 width: <?=$pData['width']?>px;
 height: <?=$pData['height']?>px;
 <?
  $pData['liMarginX'] = $liData['width']-$pData['width'];
  $pData['liMarginY'] = $liData['height']-$pData['height'];
 ?>
 margin: <? echo pData['liMarginY']/2; ?>px <? echo pData['liMarginX']/2; ?>px;
}
