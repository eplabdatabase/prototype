<!DOCTYPE html>
<html>
<head>
<?php require "templates/header.php"; ?>
<style>
body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1 {
    color: black;
    padding: 10px 10px 10px;
    text-align: center;
}
/*  Add new - green*/
a.class1:link, a.class1:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #3ec187; /* green*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 40px 25px 25px 25px;
    text-align: center;
    text-decoration: none;
}

a.class1:hover, a.class1:active {
    background-color: #32bc7f; /*darker reen*/
}
/*  View / Modify - purple*/
a.class2:link, a.class2:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #c3a3ce; /*purple*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 30px 25px;
    text-align: center;
    text-decoration: none;
}

a.class2:hover, a.class2:active {
    background-color: #ae92b7; /*darker purple*/
}
/*  Dashboard - light blue*/
a.class3:link, a.class3:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #80d6e5; /*blue*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 30px 25px;
    text-align: center;
    text-decoration: none;
}

a.class3:hover, a.class3:active {
    background-color: #6ec4d3; /*darker blue*/
}
a.class4:link, a.class4:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #ff9191; /*red*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 35px 20px;
    text-align: center;
    text-decoration: none;
}

a.class4:hover, a.class4:active {
    background-color: #ea4d4d; /*darker red*/
}
a.class5:link, a.class5:visited {

    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #ffc6f9; /*pink*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 35px 25px;
    text-align: center;
    text-decoration: none;
}

a.class5:hover, a.class5:active {
    background-color: #ffaff6; /*darker pink*/
}
a.class6:link, a.class6:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #ffdda0; /*orange*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 30px 25px;
    text-align: center;
    text-decoration: none;
}
a.class6:hover, a.class6:active {
    background-color: #ffd58c; /*darker orange*/
}
a.class7:link, a.class7:visited {
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    background-color: #f9ff91; /*yellow*/
    color: black;
    border-style: solid;
    border-color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
}
a.class7:hover, a.class7:active {
    background-color: #e6ed78; /*darker yellow*/
}
.flex-container {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;

}
.flex-container > a {
  margin: 20px;
}



p {
    font-family: verdana;
    font-size: 20px;
}

>
</style>
</head>

<body>

<div class = "flex-container">
<a href="newsubject.php"class=class1>Add New Subject<br/></a>
<a href="existingData.php"class=class2>View & Modify <br/>Existing Data<br/></a>
<a href="studyview.php"class=class3>Project Dashboard<br/>(Study View)<br/></a>
<a href="bioSpecs.php"class=class4>View Biological<br/>Speciman Data<br/></a>
<a href="clinMeasures.php"class=class5>View Clinical<br/>Measure Data<br/></a>
<a href="neuroMeasures.php"class=class6>View<br/>Neurophysiology<br/>Data<br/></a>
<a href="studyview.php"class=class7>Export Data<br/></a>



</div>
</body>
<?php require "templates/footer.php"; ?>
</html>
