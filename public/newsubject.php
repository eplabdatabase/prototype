<!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #3ec187;
  align-self: center;
  text-align: center;
}
div{
  font-size: 20px;
  font-weight: bold;
  background-color: white;
  text-align: left;
  align-items: start;
  align-content: center;
  padding-left: 350px;
}

a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 400px;
}

</style>
</head>
<body>
<h1>Add New Subject</h1>
</body>
<?php

 // Use an HTML form to create a new entry in the users table.

function unique_random($table, $col, $min, $max){
   $unique = false;
     $tested = []; // Store tested results in array to not test them again
     do{
         $random = random_int($min,$max); // Generate random string of characters
         if( in_array($random, $tested) ){ // Check if it's already testing - If so, don't query the database again
             continue;
         }
         //$count = DB::table($table)->where($col, '=',$random)->count(); // Check if it is unique in the database
				 $count = 0;
				 $tested[] = $random; // Store the random character in the tested array to keep track which ones are already tested
         if( $count == 0){ // generated number appears to be unique
             $unique = true; // Set unique to true to break the loop
         }
     } while(!$unique);// If unique is still false,repeat until it has generated a random number
     return $random;
}

if (isset($_POST['submit'])){
	require "../config.php";
	require "../common.php";
	try	{
		$connection = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo"); //, $username, $password, $options);
    $sql = "SELECT uniBDNPID FROM subjectInfo";
    $statement = $connection->query($sql);
    $statement->execute();
    $result = $statement->fetchAll();
     foreach ($result as $row){
       if ($row = max($result)){
         $value = $row["uniBDNPID"];
         // echo $value;
       }
     }

    // sprintf("%02d", $uniBDNPID);
		$new_subject = array(
      "uniBDNPID"    => sprintf("%05d", $value+1),
			"subjectFirst" => $_POST['subjectFirst'],
			"subjectLast"  => $_POST['subjectLast'],
			"subjectID"		 => $_POST['subjectID'],
			"dob"       	 => $_POST['dob'],
			// "study"				 => $_POST['study'],
			"parentFirst"  => $_POST['parentFirst'],
			"parentLast"   => $_POST['parentLast'],
			"email"     	 => $_POST['email'],
		);

		$sql = sprintf(
				"INSERT INTO %s (%s) values (%s)",
				"subjectInfo",
				implode(", ", array_keys($new_subject)),
				":" . implode(", :", array_keys($new_subject))
		);
		$statement = $connection->prepare($sql);
		$statement->execute($new_subject);
	}	catch(PDOException $error){
		echo $sql . "<br>" . $error->getMessage();
	}
}
?>

<?php
#  $uniBDNPID = unique_random('subjectInfo','uniBDNPID',1000,10000);
#  $connection = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo"); //, $username, $password, $options);
#  $sql2 = "UPDATE subjectInfo SET uniBDNPID = $uniBDNPID WHERE subjectID = 1234"
#  $statement = $connection->prepare($sql2);
#  $statement->execute();
?>


<?php
if (isset($_POST['submit']) && $statement)
{ ?>
	<blockquote><?php echo $_POST['subjectFirst']; ?> successfully added.<br/>
		<!-- Deidentification number: //php echo $uniBDNPID; ?> -->
	</blockquote>
<?php
} ?>

<div>
<form method="post">
	<label for="subjectFirst">First Name</label>
	<input type="text" name="subjectFirst" id="subjectFirst"><br/><br/>
	<label for="subjectLast">Last Name</label>
	<input type="text" name="subjectLast" id="subjectLast"><br/><br/>
	<label for="subjectID">Subject ID</label>
	<input type="text" name="subjectID" id="subjectID"><br/><br/>
	<label for="dob">Date of Birth</label>
	<input type="date" name="dob" id="dob"><br/><br/>
	<label for="study">Study</label>
	<select type="text" name="study" id="study"><br/><br/>
	  <option value="u54proj1">U54 Project 1</option>
	  <option value="u54proj2">U54 Project 2</option>
	  <option value="k23">K23 FXS TMS</option>
	  <option value="ASA">AS Acamprosate</option>
		<option value="ASC">AS Control</option>
		<option value="ketamine">Ketamine</option>
		<option value="azd">AZD</option>
		<option value="neuroNext">Neuro Next</option>
		<option value="ddnr">DDNR</option>
	</select><br/><br/>
	<label for="parentFirst">Parent First Name</label>
	<input type="text" name="parentFirst" id="parentFirst"><br/><br/>
	<label for="parentLast">Parent Last Name</label>
	<input type="text" name="parentLast" id="parentLast"><br/><br/>
	<label for="email">Email</label>
	<input type="email" name="email" id="email"><br/><br/>

	<input type="submit" name="submit" value="Submit"><br/><br/>
</form>
</div>

<a href="home.php">Back to home</a>

<?php require "templates/footer.php"; ?>
