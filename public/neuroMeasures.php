
<!DOCTYPE html>
<html>
<head>
<style>
h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #ffc654;
  align-self: center;
  text-align: center;
}
a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 570px;
}
#neuroMeasures {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#neuroMeasures td, #neuroMeasures th {
    border: 1px solid #ddd;
    padding: 8px;
}


#neuroMeasures tr:hover {background-color: #ddd;}

#neuroMeasures th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #ffd37c;
    color: black;
}

</style>
<h1>Neurophysiology Measures</h1>
</head>

<body>
<table id="neuroMeasures">
  <tr>
    <th>BDNPID</th>
    <th>First Name</th>
    <th>Last Name</th>
		<th>Study</th>
		<th>EEG Collected?</th>
		<th>Resting Deidentified ID</th>
		<th>Chirp Deidentified ID</th>
		<th>Habituation Deidentified ID</th>
		<th>Auditory ERP Deidentified ID</th>
    <th>Visual ERP Deidentified ID</th>
    <th>Tactile ERP Deidentified ID</th>
    <th>Eye Tracking Collected?<th/>
  </tr>

<?php

    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT *
             FROM neurophys";
    $statement = $conn->query($sql);
  //$result = $statement->fetchAll();
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><?php echo ($row["uniBDNPID"]); ?></td>
  				<td><?php echo ($row["subjectFirst"]); ?></td>
  				<td><?php echo ($row["subjectLast"]); ?></td>
					<td><?php echo ($row["study"]); ?></td>
					<td><?php echo ($row["collected"]); ?></td>
					<td><?php echo ($row["rest"]); ?></td>
          <td><?php echo ($row["chirp"]); ?></td>
					<td><?php echo ($row["hab"]); ?></td>
					<td><?php echo ($row["auditory"]); ?></td>
					<td><?php echo ($row["visual"]); ?></td>
					<td><?php echo ($row["tactile"]); ?></td>
          <td><?php echo ($row["eyetrack"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>
<br/>
<a href="home.php">Back to home</a>

<?php require "templates/footer.php"; ?>

</html>
