<!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #ffd3fa;
  align-self: center;
  text-align: center;
}

#measures {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#measures td, #measures th {
    border: 1px solid #ddd;
    padding: 8px;
}


#measures tr:hover {background-color: #ddd;}

#measures th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #ffd3fa;
    color: black;
}

a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 400px;
}

</style>
</head>

<body>
<h1>Clinical Measures</h1>
<table id="measures">
  <tr>
    <th>Project</th>
    <th>IRB</th>
    <th># of Subjects</th>
  </tr>

<?php
    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT project, irbNum,subjectCount
             FROM studyView";
    $statement = $conn->query($sql);
  //$result = $statement->fetchAll();
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><?php echo ($row["project"]); ?></td>
  				<td><?php echo ($row["irbNum"]); ?></td>
  				<td><?php echo ($row["subjectCount"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>



</body>
<a href="home.php">Back to home</a>
<?php require "templates/footer.php"; ?>
</html>
