<!DOCTYPE html>
<html>
<head>
<style>

#myDIV  {
    background-color: white;
    position: absolute;
    height: 830px;
    width: 200px;
    left: 500px;
}

body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #6ec4d3;
  align-self: center;
  text-align: center;
}
h2{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #80d6e5;
  align-self: center;
  text-align: center;
}

div.class1{
  border-style: solid;
  border-width: 1px;
  left: 600px;
  position: relative;
  bottom:700px;
  width: 150px;

}

#table2 {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 40%;
    bottom:900px;
    left:600px;
    position: relative;
    border-style: solid;
    border-width: 3px;
}

#table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 50%;
}

#table td, #table th {
    border: 1px solid #ddd;
    padding: 8px;
}


#table tr:hover {background-color: #ddd;}

#table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #80d6e5;
    color: black;
}

a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 400px;
}

</style>
</head>

<body>


<h1>Study Snapshot</h1>
<script>
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
<h2>John Merck Acamprosate 2013-2293</h2>
<button onclick="myFunction()">Show Subject Names</button>

<table id="table">
  <tr>
    <th>Study ID</th>
    <th>Universal ID</th>
    <th>Name</th>
  </tr>
  <div id="myDIV"> </div>
<?php
    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT studyID, uniID, name
             FROM JMstudy";
    $statement = $conn->query($sql);
  //$result = $statement->fetchAll();
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><?php echo ($row["studyID"]); ?></td>
  				<td><?php echo ($row["uniID"]); ?></td>
  				<td ><?php echo ($row["name"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>


<table id="table2">
  <tr>
    <th>Subjects Enrolled: 24</th>
    <th>Subjects Completed: 22</th>
    <th>Subjects with Missing Data: 2</th>
  </tr>


<div class=class1>Export Study Data</div>
<div class=class1>View/Filter Subjects</div>
<div class=class1>View Measures</div>
<div class=class1>Query Data</div>

</body>
<a href="home.php">Back to home</a>
<?php require "templates/footer.php"; ?>
</html>
