<!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #80d6e5;
  align-self: center;
  text-align: center;
}

#projects {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#projects td, #projects th {
    border: 1px solid #ddd;
    padding: 8px;
}


#projects tr:hover {background-color: #ddd;}

#projects th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #80d6e5;
    color: black;
}

a.class1 {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 400px;
}
a.class2{
  color:black;
  font-size: 20;
}
</style>
</head>

<body>

<h1>Project Dashboard - Study View</h1>
<table id="projects">
  <tr>
    <th>Project</th>
    <th>IRB</th>
    <th># of Subjects</th>
  </tr>


<?php
    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT project, irbNum,subjectCount
             FROM studyView";
    $statement = $conn->query($sql);
  //$result = $statement->fetchAll();
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><a href="acamprosate.php"class=class2><?php echo ($row["project"]); ?></a></td>
  				<td><?php echo ($row["irbNum"]); ?></td>
  				<td><?php echo ($row["subjectCount"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>



</body>
<a href="home.php"class=class1>Back to home</a>
<?php require "templates/footer.php"; ?>
</html>
