<!DOCTYPE html>
<html>
<head>
<style>

body {
    background-color: white;
    padding: 20px 175px 20px 175px;
}

h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #ea4d4d;
  align-self: center;
  text-align: center;
}
h2{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #ff9191;
  align-self: center;
  text-align: center;
}


#table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#table td, #table th {
    border: 1px solid #ddd;
    padding: 8px;
}


#table tr:hover {background-color: #ddd;}

#table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #ff9191;
    color: black;
}

a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 400px;
}

</style>
</head>

<body>


<h1>Biological Specimans</h1>
<h2>Plasma</h2>

<table id="table">
  <tr>
    <th>BDNPID</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Plasma Collected?</th>
    <th>Study</th>
    <th>Number of Aliquots</th>
    <th>Parent Sample ID</th>
  </tr>
<?php
    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT uniBDNPID,subjectFirst,subjectLast,collected,study,aliquots,parentID
             FROM plasma";
    $statement = $conn->query($sql);
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><?php echo ($row["uniBDNPID"]); ?></td>
  				<td><?php echo ($row["subjectFirst"]); ?></td>
  				<td ><?php echo ($row["subjectLast"]); ?></td>
          <td ><?php echo ($row["collected"]); ?></td>
          <td ><?php echo ($row["study"]); ?></td>
          <td ><?php echo ($row["aliquots"]); ?></td>
          <td ><?php echo ($row["parentID"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>

</body>
<a href="home.php">Back to home</a>
<?php require "templates/footer.php"; ?>
</html>
