
<!DOCTYPE html>
<html>
<head>
<style>
h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #c3a3ce;
  align-self: center;
  text-align: center;
}
a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 570px;
}
#subjects {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#subjects td, #subjects th {
    border: 1px solid #ddd;
    padding: 8px;
}


#subjects tr:hover {background-color: #ddd;}

#subjects th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #c3a3ce;
    color: black;
}

</style>
<h1>BDNP Repository Subjects</h1>
</head>

<body>
  <a href="subjectview.php">Search For a Subject<br/><br/></a>

<table id="subjects">
  <tr>
    <th>BDNPID</th>
    <th>First Name</th>
    <th>Last Name</th>
		<th>DDCR ID</th>
		<th>U54 ID</th>
		<th>U54 Project 1 ID</th>
		<th>U54 Project 2 ID</th>
		<th>John Merck ID</th>
		<th>John Merck Study ID</th>
		<th>Ketamine Study ID</th>
  </tr>

<?php

    $conn = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");
    if( $conn === false ){
         echo "Could not connect.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    $sql = "SELECT *
             FROM subjectInfo";
    $statement = $conn->query($sql);
  //$result = $statement->fetchAll();
    $statement->execute();
    $result = $statement->fetchAll();
  		foreach ($result as $row){?>
  			<tr>
  				<td><?php echo ($row["uniBDNPID"]); ?></td>
  				<td><?php echo ($row["subjectFirst"]); ?></td>
  				<td><?php echo ($row["subjectLast"]); ?></td>
					<td><?php echo ($row["DDCR_ID"]); ?></td>
					<td><?php echo ($row["U54_ID"]); ?></td>
					<td><?php echo ($row["U54P1_ID"]); ?></td>
					<td><?php echo ($row["U54P2_ID"]); ?></td>
					<td><?php echo ($row["JohnMerck_ID"]); ?></td>
					<td><?php echo ($row["JohnMerck_StudyID"]); ?></td>
					<td><?php echo ($row["Ketamine_StudyID"]); ?></td>
  			</tr>
      <?php
    	} ?>

</table>
<br/>
<a href="home.php">Back to home</a>

<?php require "templates/footer.php"; ?>

</html>
