
<!DOCTYPE html>
<html>
<head>
<style>
h1{
  font-size: 40px;
  border-style: solid;
  border-color: black;
  background-color: #c3a3ce;
  align-self: center;
  text-align: center;
}
h2{
  font-size: 20px;
  align-self: center;
  text-align: center;
}

a {
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    padding-left: 625px;
}
blockquote{
  text-align: center;
}
#subjects {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#subjects td, #subjects th {
    border: 1px solid #ddd;
    padding: 8px;
}


#subjects tr:hover {background-color: #ddd;}

#subjects th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #c3a3ce;
    color: black;
}

</style>
<h1>BDNP Repository Subjects</h1>
</head>

<body>

<?php

/**
 * Function to query information based on
 * a parameter: in this case, name.
 *
 */

if (isset($_POST['submit']))
{

	try
	{

		require "../config.php";
		require "../common.php";

		$connection = new PDO("sqlsrv:Server=P15-5187;Database=BDNPRepo");

		$sql = "SELECT *
						FROM subjectInfo
						WHERE CONCAT(subjectFirst, ' ', subjectLast) = :name";

		$name = $_POST['name'];

		$statement = $connection->prepare($sql);
		$statement->bindParam(':name', $name, PDO::PARAM_STR);
		$statement->execute();

		$result = $statement->fetchAll();
	}

	catch(PDOException $error)
	{
		echo $sql . "<br>" . $error->getMessage();
	}
}
?>

<?php
if (isset($_POST['submit']))
{
	if ($result && $statement->rowCount() > 0)
	{ ?>
		<h2>Results</h2>

		<table id="subjects">
			<thead>
				<tr>
					<th>BDNP ID</th>
					<th>Subject ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Date of Birth</th>
					<th>Parent First Name</th>
					<th>Parent Last Name</th>
					<th>email</th>
				</tr>
			</thead>
			<tbody>
	<?php
		foreach ($result as $row)
		{ ?>
			<tr>
				<td><?php echo escape($row["uniBDNPID"]); ?></td>
				<td><?php echo escape($row["subjectID"]); ?></td>
				<td><?php echo escape($row["subjectFirst"]); ?></td>
				<td><?php echo escape($row["subjectLast"]); ?></td>
				<td><?php echo escape($row["dob"]); ?></td>
				<td><?php echo escape($row["parentFirst"]); ?> </td>
				<td><?php echo escape($row["parentLast"]); ?> </td>
				<td><?php echo escape($row["email"]); ?> </td>
			</tr>
		<?php
		} ?>
		</body>
	</table>
	<?php
	}
	else
	{ ?>
		<blockquote>No results found for <?php echo escape($_POST['name']); ?>.</blockquote>
	<?php
	}
}?>

<h2>Enter subject name to search:

<form method="post">
	<!-- <label for="name">name</label> -->
	<input type="text" id="name" name="name">
  <br/>
	<input type="submit" name="submit" value="View Results">
</form>
</h2>
<a href="home.php">Back to home</a>

<?php require "templates/footer.php"; ?>

</html>
